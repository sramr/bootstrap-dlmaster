#!/usr/bin/env bash

echo -n "Enter gitlab access token (api level access required): "
read -s ACCESS_TOKEN
echo

########################################################################################################################
BOOTSTRAP_DL_URL="https://gitlab.com/sreeram-r86/bootstrap-dlmaster/-/archive/master/bootstrap-dlmaster-master.tar.gz?private_token=${ACCESS_TOKEN}&sha=master"
SALT_STATES_REPO_SSH="git@gitlab.com:sreeram-r86/salt-states.git"
########################################################################################################################

apt update
apt-get install -y curl salt-cloud
curl -L https://bootstrap.saltstack.com > /tmp/salt-install.sh
bash /tmp/salt-install.sh -U -x python3 stable 2019.2.3
rm -f /tmp/salt-install.sh
mkdir -p /srv/salt/
echo -e "file_client: local\nfile_roots:\n  base:\n    - /srv/salt" > /etc/salt/minion.d/00_masterless.conf
systemctl restart salt-minion
curl -s ${BOOTSTRAP_DL_URL} | tar -xzvf - --strip-components=1 --exclude='bootstrap-dlmaster-master/README.md' --exclude='bootstrap-dlmaster-master/bootstrap-dl.sh' --exclude='bootstrap-dlmaster-master/.gitignore' -C /srv/salt/

salt-call state.apply dlmaster

while [[ "$(salt-key -l unaccepted | grep -v ^Unaccepted | grep ^dlmaster$ | wc -l)" = "0" ]]; do echo "Waiting for minion..." && sleep 2; done

salt-key -ya dlmaster
salt-key -l unaccepted | grep -v ^Unaccepted | while read line; do salt-key -yd ${line}; done

rm -rf /srv/salt/*
rm -f /srv/salt/.gitignore

echo "Authorize the following public key with the salt-states and salt-pillar repos NOW:"
echo ""
cat /root/.ssh/id_rsa.pub
echo ""

until GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone ${SALT_STATES_REPO_SSH} /srv/salt/
do
  sleep 15
  echo "Waiting for key to be authorized..."
done

# to avoid redundancy between bootstrap and real states this is copied from repo and not applied by salt:
cp /srv/salt/files/master.conf /etc/salt/master.d/
systemctl restart salt-master
sleep 10
salt-cloud -u
salt dlmaster state.apply dlmaster
rm -rf /srv/salt/
echo "rebooting"
reboot
