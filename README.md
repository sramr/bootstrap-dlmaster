# bootstrap-dlmaster

The following section describes how this PoC can be used.

Some words of warning:

This is a PoC and far from production ready. Lots of pragmatic decisions were made to deliver this within a few hours of work.

Its purpose is solely to demonstrate how it can be done theoretically and test its resilience against node failure.

**(Non-exhaustive) TODO list**:
- debug spwan-npoc orchestration heisenbug
- modify NC config to use object storage instead of NFS
- test with other cloud providers (besides digitalocean) - adapt if necessary
- finish debuggin scaleway module (not working)
- add SSL to haproxy
- add credential sets to encrypted pillar data
- get rid of cmd.run usage wherever possible
- move bash execution from placed scripts (especially on master which is supposed to project agnostic) to files server directly from salt:// file roots
- ...

## Create your saltmaster vm

This example uses Hetzner's cloud and assumes you created a context already and imported a private ssh key named ``mysshkey``:

```bash
hcloud server create --image=ubuntu-18.04 --name=dlmaster --type=cx21 --ssh-key=mysshkey
```

Take note of the IP of your new system.

**HINT:** You can use any mechanism to provide a fresh VM, that's just an example.

## Bootstrap the master
```bash
ssh root@IP from previous step
# enter credentials of your account during clone below
git clone https://gitlab.e.foundation/e/infra/scalable-ecloud-poc/bootstrap-dlmaster.git /tmp/bs
bash /tmp/bs/bootstrap-dl.sh 
```

During the installation you will see some output similar to that one:
```bash
[...]
Authorize the following public key with the salt-states and salt-pillar repos NOW:

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7Lr144uWHw/hS1z0Zll4/ku2MNgMwpheimgHL/okUI/1sSFQ5qVTRY2ViTYBw2rSkE/uDU8MVVIFcrjNhB+az0UNVyhYfr3uc/C3j5TNizlv8XeokptBlZyd6zVQKpL8SkYWeJvaJrw3nL62Bo70wzybpQOy+wwU/Zlmp7/qh8FncVx/f6Tlf5jeNNKMNQZC1MOpRPttw1w/C7YVJt6DrgQcfz8m4Yi/QAEx/x85idRRbnbOs/z9YDWdxf1JVw4Xu5XQfj9KDug/5CnMNmaX0ZR3nPo4Qjj/9DTfygcG4feSazW4UrH5P71B+f6xBdyMgwVOgOsEU5KopY8yiTnM7 root@dlmaster`

Cloning into '/srv/salt'...
[...]
```
That means you need to authorize the ssh pub key from the output with the git repos (``salt-pillar``, ``salt-states``).
Additionally you can already update the named SSH keys with the provider accordingly.

The above command should end in an aborted connection (due to a reboot). Log in again after the box rebooted.

## General master usage info

If you intend to use your current master to spawn the ``scalable-ecloud`` PoC **DO NOT** run the commands below **BEFORE** you spawned the PoC.
Afterwards you can safely run the below commands to spawn additional VMs.

Test that the minion of the salt master itself is functional and returns ``True`` upon a ping:
```bash
salt \* test.ping
```

Spawn a VM in the wireguard network with ID ``someminionid``:
```bash
salt-run state.orch orchestration.spawn-vm pillar='{"serverid": "someminionid" }'
```
Destroy a VM in the wireguard network with ID ``someminionid``:
```bash
salt-cloud -d someminionid
```

## Spawn scalable-ecloud PoC

**NOTE**: This is for now only tested with the digitalocean cloud provider. Generally the provider should not make any difference however the name of the primary interface might be different which might cause problems.
As it is a PoC I did not invest yet into making it generic on the primary interface name (yet).


You'll find details and specifics for the various supported providers here: https://docs.saltstack.com/en/latest/topics/cloud/index.html#cloud-provider-specifics

Procedure:
- First save the private key (**TBD** where to get the private key from? For now ask Thilo/Arnau for it...) in file ``~/exported_private.key``
- Import the key and remove the file again:
```bash
mkdir -p /etc/salt/gpgkeys
chmod 0700 /etc/salt/gpgkeys
gpg --homedir /etc/salt/gpgkeys --import ~/exported_private.key
rm ~/exported_private.key
# List to verify import
gpg --homedir /etc/salt/gpgkeys -k
```

Check that the secrets are decrypted (no PGP message visible in the output):
```bash
salt-call pillar.items
```
**NOTE:** If necessary (if you are the first one to run an instance of the PoC) check the "MUST READ" section below first before going further.

```diff
- The below spawn-npoc orchestration has a heisenbug, works sometimes but not always 
- Fallback to the multi-liner two blocks further below for a more reliable spawning for now...
```
This spawns a PoC based on the node configuration from ``orchestration/ecorp/map.jinja``:
```bash
salt-run state.orch orchestration.ecorp.spawn-ncpoc
```

**HINT**: Now go an grab some coffee. With three DB and two APP nodes this takes around 60-70 minutes. (See TODO in regards to parallelisation)

Once completed retrieve the public IP of your HAproxy to connect to your Nextcloud instance:
```bash
/root/scripts/gethaproxypubip.sh
```

**NOTE:** Use this instead of the orchestration for now. --- Just for documentation purpose - this the single steps that the above orchestration executes in order - might be useful if you want to spawn just a Galera cluster for instance:
```bash
salt-run state.orch orchestration.spawn-vm pillar='{"serverid": "dbnode1" }'
salt-run state.orch orchestration.spawn-vm pillar='{"serverid": "dbnode2" }'
salt-run state.orch orchestration.spawn-vm pillar='{"serverid": "dbnode3" }'
salt-run state.orch orchestration.spawn-vm pillar='{"serverid": "ncnode1" }'
salt-run state.orch orchestration.spawn-vm pillar='{"serverid": "ncnode2" }'
salt-run state.orch orchestration.spawn-vm pillar='{"serverid": "haproxy" }'
salt dbnode* state.apply galera-node
salt dbnode1 cmd.run "galera_new_cluster"
salt dbnode* cmd.run "systemctl enable --now mariadb"
salt dbnode1 cmd.run "mysql -u root -e \"SHOW STATUS LIKE 'wsrep_cluster_size'\""
salt dbnode1 cmd.run "mysql -u root -e 'CREATE DATABASE nc;'"
salt dbnode1 cmd.run "mysql -u root -e  \"CREATE USER 'ncuser'@'%' IDENTIFIED BY 'ncuser';\""
salt dbnode1 cmd.run "mysql -u root -e  \"GRANT ALL PRIVILEGES ON nc.* TO 'ncuser'@'%';\""
salt ncnode1 state.apply nextcloud-node
salt ncnode2 state.apply nextcloud-node
salt haproxy state.apply haproxy-node
salt-run state.orch orchestration.ecorp.update-haproxy
salt-run state.orch orchestration.ecorp.trust-haproxy-pubip
```

## Restart from scratch with scalable-ecloud PoC (using existing master)

```bash
# delete all unrelated VMs if any
# delete all related VMs (except wg-server):
salt-cloud -d ncnode1 ncnode2 dbnode1 dbnode2 dbnode3 haproxy
# Lookup public IP of wg-server and use it in the next steps, example value below is 167.172.43.210
salt wg-server grains.get ip_interfaces
IP=167.172.43.210
# reset wireguard setup
ssh $IP "wg-quick down wg0"
ssh $IP "head -n8 /etc/wireguard/wg0.conf > /tmp/wg0.conf && mv /tmp/wg0.conf /etc/wireguard/wg0.conf"
ssh $IP "wg-quick up wg0"
wg-quick down wg0
/root/scripts/peer-master-with-wgserver.sh
ssh $IP "systemctl restart salt-minion"
# Clear NFS share
rm -rf  /srv/data/nfsshare/* /srv/data/nfsshare/.htaccess /srv/data/nfsshare/.ocdata
# Check functionality (minion might need up to a minute to start):
salt \* test.ping
```

### MUST READ for the "first timer"
If you are the first person to use the PoC code for the /e/ foundation you
need to replace all the example values in the provider configuration in the ``salt-pillar``
repository with real values (``dlmaster/providers.sls``) for the /e/ cloud provider accounts.

**HINT:** If you come across values that should not be in git in plaintext encrypt them using pgp
using the public key from the ``salt-pillar`` repository (file ``exported_pubkey.gpg``).

**HINT2:** The currently used VM profiles (``dlmaster/profiles.sls``) only reference the digitalocean provider (atm).

Import the pubkey (found here https://gitlab.e.foundation/e/infra/scalable-ecloud-poc/salt-pillars/-/raw/master/exported_pubkey.gpg):
```bash
gpg --import exported_pubkey.gpg

# Encrypt secrets (**HINT:** unset HISTFILE before to avoid having secrets in shell history):
echo -n "supersecret" | gpg --armor --batch --trust-model always --encrypt -r "root@dlmaster"
```
**HINT:**

When putting gpg message blocks into the pillar mind the correct yaml indentation,
this can easily be done by appending the correct amount of whitespaces while encrypting using this command:
```bash
echo -n "supersecret" | gpg --armor --batch --trust-model always --encrypt -r "root@dlmaster" | sed 's@^@        @g'
```

List pillars on master to verify the decrypted values:
```bash
salt-call pillar.items
```





