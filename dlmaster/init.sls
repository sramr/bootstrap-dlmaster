base-packages-installed:
  pkg.installed:
    - pkgs:
      - salt-master
      - salt-minion
      - salt-cloud
      - python3-pygit2

dlmaster-running:
  service.running:
    - name: salt-master
    - enable: true
    - require:
      - base-packages-installed

dlmaster-minion-running:
  service.running:
    - name: salt-minion
    - enable: true
    - require:
      - base-packages-installed
      - masterless-config-removed
      - dlmaster-minion-configured
    - watch:
      - dlmaster-minion-configured

masterless-config-removed:
  file.absent:
    - name: /etc/salt/minion.d/00_masterless.conf

dlmaster-minion-configured:
  file.managed:
    - name: /etc/salt/minion.d/minion.conf
    - source: salt://files/minion.conf

ssh-keypair-root-generated:
  cmd.run:
    - name: ssh-keygen -q -N '' -f /root/.ssh/id_rsa
    - unless: test -f /root/.ssh/id_rsa
